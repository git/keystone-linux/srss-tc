# Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
#
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

SRSS_ROOT = $(shell pwd)
SRSS_SRC = $(SRSS_ROOT)/src
export SRSS_BIN ?= $(SRSS_ROOT)/bin
export SRSS_INC = $(SRSS_ROOT)/include

ARCH = arm
CROSS_COMPILE ?= arm-linux-gnueabihf-
ARFLAGS = crus
LDFLAGS ?= -Wl,--hash-style=gnu
CFLAGS ?= -march=armv7-a -marm -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a15 -O3
EXTRA_CFLAGS = -g -ggdb2 -D_GNU_SOURCE -DARCH_$(ARCH)
CC = $(CROSS_COMPILE)gcc $(CFLAGS) $(EXTRA_CFLAGS)
AR = $(CROSS_COMPILE)ar

TARGET_PLATFORM = $(ARCH)
LDFLAGS += -pthread -lrt

export TARGET_PLATFORM
export LDFLAGS
export ARFLAGS
export CC
export AR
export LD

MAKEFLAGS += --no-print-directory

.PHONY: all build clean srss

all: srss

srss:
	make -C $(SRSS_SRC) all

clean:
	make -C $(SRSS_SRC) clean


