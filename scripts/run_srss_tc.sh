#!/bin/sh

compatible=$(cat /proc/device-tree/compatible)
case "$compatible" in
  *k2hk-evm*) 
	run=0
	platform=k2hk-evm
	i2_bus=2
	i2c_addr=27
	;;
  *k2l-evm*)  
	platform=k2l-evm
	run=1
	i2c_bus=2
	i2c_addr=9
	;;
  *k2e-evm*)  
	platform=k2e-evm
	run=0
	i2c_bus=1
	i2c_addr=9
	;;
  *)	
	platform=unknown
	run=0
	;;
esac

echo platform: $platform
if [ "$run" = "1" ]; then
	srss_tc.out -i $i2c_bus -p $i2c_addr &
fi

