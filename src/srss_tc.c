/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>
#include "i2c-dev.h"

#define SRSS_TC_VERSION		"1.0.0.0"
#define SRSS_DEVICE		"srss"

/* Smart Reflex Sub-System register offset */
#define SRSS_PID		0x0
#define SRSS_INTR_EN_SET	0x4
#define SRSS_EVENT_FLAG		0x8
#define SRSS_EVENT_CLR		0xc
#define SRSS_TEMP_CTL0		0x1c
#define SRSS_TEMP_CTL1		0x20
#define SRSS_TEMP_CTL2		0x24
#define SRSS_TEMP_STAT0		0x28
#define SRSS_TEMP_STAT1		0x2c
#define SRSS_VPRM_CTL0		0x80
#define SRSS_VPRM_CTL1		0x84
#define SRSS_VPRM_STAT0		0x88
#define SRSS_VPRM_STAT1		0x8c

/* SRSS_TEMP_CTL0 fields definitions */
#define C0TEMP_ENABLE		(0x1 << 2)	/* enables C0 TC */

/* SRSS_VPRM_CTL0 fields definitions */
#define VPRM_ENABLE		(0x1 << 0)	/* enables SR C0 */
#define VPRM_DISABLE		(0x0 << 0)	/* disable VPRM FSM */
#define SMPS_MODE_VID		(0x2 << 2)	/* VID 6-bit */
#define SMPS_MODE_I2C		(0x3 << 2)	/* I2C 6-bit */

/* SRSS temperature events */
#define SRSS_EVENT_THRPT	(1 << 17)	/* temp threshold point */

/* SoC default I2C bus to communicate with the exteranl power controller */
#define I2C_BUS_NUM		2

/* external power controller I2C slave device address */
#define PC_I2C_ADDR			9

#define I2C_RETRY_COUNT		5

#define MAX_NAME_LENGTH		64
#define MAX_FILE_NAME_LENGTH	128
#define MAX_LINE_LENGTH         16

/* Implementation of check for option argument correctness.  */
#define assert_arg(A) \
          if (++i == argc || ((arg = argv[i])[0] == '-' && \
              !isdigit ((int)arg[1]) )) \
            { \
              srss_log(LOG_STDOUT | LOG_FILE, "missing argument to option '%s'\n", A); \
              exit (1); \
            }

/* SRSS logging definitions */

/* log type */
#define LOG_STDOUT		1
#define LOG_FILE		2

#define MAX_PRE_LOG_BUF_LEN	4096
#define DEFAULT_LOG_LEN		0x800000
#define SRSS_LOG_FILE_NAME	"/var/log/srss.log"

static int debug = 0;

/* srss logging utility */
static char srss_log_buf[MAX_PRE_LOG_BUF_LEN];
static FILE *srss_log_fp = NULL;
void srss_log(int log_type, const char* format, ...)
{
	int         len;
	va_list     args;
	struct stat fbuf;

	srss_log_buf[0] = 0;

	len = MAX_PRE_LOG_BUF_LEN - strlen(srss_log_buf);
	if (len <= 0)
		return;

	va_start(args, format);
	vsnprintf(&srss_log_buf[strlen(srss_log_buf)], len, format, args);
	va_end(args);

	if (srss_log_fp && (log_type & LOG_FILE)) {
		/* logfile reset if going over max_len */
		fstat(fileno(srss_log_fp), &fbuf);
		if ((fbuf.st_size + strlen(srss_log_buf)) > DEFAULT_LOG_LEN)
			freopen(SRSS_LOG_FILE_NAME, "w+", srss_log_fp);

		fprintf(srss_log_fp, "%s", srss_log_buf);
		fflush(srss_log_fp);
	}
	if (log_type & LOG_STDOUT) {
		fprintf(stdout, "%s", srss_log_buf);
		fflush(stdout);
	}
}

inline uint32_t srss_read_reg(int fd, uint32_t addr)
{
	uint32_t data;

	pread(fd, &data, sizeof(uint32_t), (off_t)addr);
	return data;
}

inline uint32_t srss_write_reg(int fd, uint32_t *data, uint32_t addr)
{
	pwrite(fd, data, sizeof(uint32_t), (off_t)addr);
	return 0;
}

static uint32_t srss_c0_tc_enabled(int fd_srss, uint32_t addr)
{
	if ((srss_read_reg(fd_srss, addr + SRSS_VPRM_CTL0) & 0xf) == \
	    (VPRM_ENABLE | SMPS_MODE_VID))
		if (srss_read_reg(fd_srss, addr + SRSS_TEMP_CTL0)
		    & C0TEMP_ENABLE)
			return 1;
	return 0;
}


static void srss_c0_tc_interrupt(int fd_srss, uint32_t addr, uint32_t enable)
{
	uint32_t data = 0;

	if (enable)
		data = 1 << 17;
	srss_write_reg(fd_srss, &data, addr + SRSS_INTR_EN_SET);
}

#define PMBUS_VOUT_COMMAND		0x21
#define PMBUS_READ_VOUT			0x8b
static uint32_t vout_wr = 0;
/* Vout in PMBus format, Vout (Volt) = vout_pmbus[vid]/512,
 * e.g. vid = 0, Vout = 0x0166 / 512 = 0.699 (Volt) */
static int srss_c0_tc_isr(int fd_srss, uint32_t addr, int fd_i2c)
{
	uint32_t vid, srss_event, data, i;
	uint8_t vout_pmbus[64][2] = {
		{0x66, 0x1}, {0x69, 0x1}, {0x6d, 0x1}, {0x70, 0x1},
		{0x73, 0x1}, {0x76, 0x1}, {0x79, 0x1}, {0x7d, 0x1},
		{0x80, 0x1}, {0x84, 0x1}, {0x87, 0x1}, {0x8a, 0x1},
		{0x8d, 0x1}, {0x90, 0x1}, {0x94, 0x1}, {0x97, 0x1},
		{0x9a, 0x1}, {0x9e, 0x1}, {0xa1, 0x1}, {0xa4, 0x1},
		{0xa7, 0x1}, {0xab, 0x1}, {0xae, 0x1}, {0xb1, 0x1},
		{0xb5, 0x1}, {0xb8, 0x1}, {0xbb, 0x1}, {0xbe, 0x1},
		{0xc2, 0x1}, {0xc5, 0x1}, {0xc8, 0x1}, {0xcb, 0x1},
		{0xcf, 0x1}, {0xd2, 0x1}, {0xd6, 0x1}, {0xd9, 0x1},
		{0xdc, 0x1}, {0xdf, 0x1}, {0xe2, 0x1}, {0xe6, 0x1},
		{0xe9, 0x1}, {0xec, 0x1}, {0xf0, 0x1}, {0xf3, 0x1},
		{0xf6, 0x1}, {0xf9, 0x1}, {0xfc, 0x1}, {0x00, 0x2},
		{0x03, 0x2}, {0x07, 0x2}, {0x0a, 0x2}, {0x0d, 0x2},
		{0x10, 0x2}, {0x13, 0x2}, {0x17, 0x2}, {0x1a, 0x2},
		{0x1d, 0x2}, {0x21, 0x2}, {0x24, 0x2}, {0x27, 0x2},
		{0x2b, 0x2}, {0x2e, 0x2}, {0x31, 0x2}, {0x34, 0x2}
	};

	srss_event = srss_read_reg(fd_srss, addr + SRSS_EVENT_FLAG);
	if (debug)
		srss_log(LOG_STDOUT | LOG_FILE, "\n SRSS Event flag reg 0x%x\n", srss_event);

	if (srss_event & SRSS_EVENT_THRPT) {
		/* temperature thresh point interrupt event */
		vid = (srss_read_reg(fd_srss, addr + SRSS_TEMP_STAT1) >> 24)
			& 0x3f;

		if (debug)
			srss_log(LOG_STDOUT | LOG_FILE, "\n SRSS temp stat0 0x%x, stat1 0x%x.\n",
				srss_read_reg(fd_srss, addr + SRSS_TEMP_STAT0),
				srss_read_reg(fd_srss, addr + SRSS_TEMP_STAT1));

		for (i = 0; i < I2C_RETRY_COUNT; i++) { 
			if (i2c_smbus_write_i2c_block_data(fd_i2c,
				PMBUS_VOUT_COMMAND, 2, vout_pmbus[vid]) == 0)
				break;
		}
		if (i == I2C_RETRY_COUNT) {
			srss_log(LOG_STDOUT | LOG_FILE, "\n Error sending VOUT_COMMAND "
				"PMBus command.\n");
			return -1;
		}

		if (debug) {
			srss_log(LOG_STDOUT | LOG_FILE, "\n Vout write: %x %x, vid: 0x%x\n",
				vout_pmbus[vid][0], vout_pmbus[vid][1], vid);
			vout_wr = (vout_pmbus[vid][1] << 8) + vout_pmbus[vid][0];
		}

		/* clear the temperature thresh point interrupt event */
		data = SRSS_EVENT_THRPT;
		srss_write_reg(fd_srss, &data, addr + SRSS_EVENT_CLR);
	}
	return 0;
}

static int uioutil_get_string (char *filename, char *str, int str_len)
{
	FILE *fpr = 0;
	int ret_val = -1;

	if (!filename || !str || !str_len) {
		goto close_n_exit;
	}

	fpr = fopen(filename, "r");
	if (!fpr) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n Error opening file %s (%s)",
		       filename, strerror(errno));
		goto close_n_exit;
	}
	if (!fgets(str, str_len, fpr)) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n Error reading file %s (%s)",
		       filename, strerror(errno));
		goto close_n_exit;			
	}

	ret_val = 0;

close_n_exit:
	if (fpr)
		fclose(fpr);
	return ret_val;
}

static int endian_check() {
    int check = 1;
    char ret = *(char *) &check;
    return ret;
}

static int byte_swap(int x)
{
	return ( ((x>>0) & 0xff) << 24)
		 | ( ((x>>8) & 0xff) << 16)
		 | ( ((x>>16) & 0xff) << 8)
		 | ( ((x>>24) & 0xff) << 0);
}

int uioutil_get_ints(char *file_name, int *buf, int size)
{
	FILE *fp;
	int i, ret = -1;

	fp = fopen(file_name, "rb");
	if (!fp) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n Error opening file %s (%s)",
		       file_name, strerror(errno));
		return -1;
	}

	if (!fread(buf, sizeof(int), size, fp)) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n Error reading file %s (%s)",
		       file_name, strerror(errno));
		fclose(fp);
		return -1;
	}
	if (endian_check())
		for (i = 0; i < size; i++)
			buf[i] = byte_swap(buf[i]);

	fclose(fp);
	return 0;
}

char  *remove_prefix_from_device_name(char *name, int max_length)
{ 
	int index = 0;

	/* strip anything before . in the name */
	while(name[index] != '\0') {
		if(name[index] == '.') {
			index++;
			return(&name[index]);
		}
		if(index > max_length)
			break;
		index++;
	}
	return(name);
}

char  *remove_postfix_from_device_name(char *name, int max_length)
{ 
	int index = 0;
  
	/* strip anything after . in the name */
	while(name[index] != '\0') {
		if(name[index] == '.') { 
			name[index] = '\0';
			index++;
			return(&name[0]);
		}
		if(index > max_length)
			break;
		index++;
	}
	return(name);
}

int uioutil_get_device (char *uio_name, char *class_name, int class_name_length)
{
	struct dirent *entry = 0;
	char filename[MAX_FILE_NAME_LENGTH];
	char name[MAX_NAME_LENGTH];
	int ret_val = -1;
	DIR *dir = 0;
        char *name_ptr;

	if (!uio_name || !class_name || !strlen(uio_name)) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n Error in call to function");
		goto close_n_exit;
	}

	dir = opendir("/sys/class/uio");
	if (!dir) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n readdir /sys/class/uio");
		goto close_n_exit;
	}

	while ((entry = readdir(dir)) != NULL) {
		if (strstr (entry->d_name, "uio") == NULL) {
			continue;
		}

		snprintf(filename, MAX_FILE_NAME_LENGTH, "/sys/class/uio/%s/name", entry->d_name);

		if (uioutil_get_string(filename, name, MAX_NAME_LENGTH) < 0)
			goto close_n_exit;
                name_ptr = remove_postfix_from_device_name(name, MAX_NAME_LENGTH);
		if (!strncmp (uio_name, name_ptr, strlen(uio_name))) {
			ret_val = 0;
			strncpy (class_name, entry->d_name, class_name_length);
			goto close_n_exit;
		}
	}

close_n_exit:
	if (dir) closedir(dir);

	return ret_val;
}

/**
 *  @brief Function clock_diff returns difference between two timeval
 *  @param[in]     start            start time
 *  @param[in]     end              end time
 *  @retval        deltaTime
 */
static int64_t clock_diff (struct timespec *start, struct timespec *end)
{
    return (int64_t)(end->tv_sec - start->tv_sec) * 1000000000L
             + end->tv_nsec - start->tv_nsec;
}

void version (char *prog_name)
{
	char *mesg = "%s %s\n";

	srss_log(LOG_STDOUT, mesg, prog_name, SRSS_TC_VERSION);
	exit(1);
}

void usage(char *prog_name)
{
	char *mesg =
		"`%s' handles Smart Reflex class 0 temperature compensation\n\n"
		"Usage: %s [OPTION [ARG]] ...\n"
		" -?, --help         show this help statement\n"
		"     --version      show version statement\n"
		" -v, --verbose      be verbose\n"
		" -i, --i2c N        SoC I2C bus number\n"
		" -p, --pc_addr N    external power controller I2C slave device address\n"
		"Example: %s --i2c 2 --pc_addr 9 --verbose\n\n";

	srss_log(LOG_STDOUT, mesg, prog_name, prog_name, prog_name);

	exit(1);
}

int main(int32_t argc, int8_t **argv)
{
	int fd_srss, fd_uio, fd_i2c;
	fd_set fds;
	struct timespec tp_end1, tp_end2;
	char file_name[MAX_FILE_NAME_LENGTH];
	char class_name[MAX_FILE_NAME_LENGTH];
	char dev_name[MAX_NAME_LENGTH];
	char *srss_dev = SRSS_DEVICE;
	int ret, i;
	uint32_t base_addr, value, delta_time, vout_rd, vout_diff;
	int mem[2], i2c_num = I2C_BUS_NUM, slave_addr = PC_I2C_ADDR;
	uint8_t vout_rb[2];

	/* Read parameters from command line */
	for (i = 1; i < argc; i++) {
		char *arg = argv[i];
		if (strcmp(arg, "--help") == 0 || strcmp(arg, "-?") == 0)
			usage(argv[0]);
		else if (strcmp(arg, "--version") == 0)
			version(argv[0]);
		else if (strcmp(arg, "--verbose") == 0 || strcmp(arg, "-v") == 0)
			debug = 1;
		else if (strcmp(arg, "--i2c") == 0 || strcmp(arg, "-i") == 0) {
			assert_arg("--i2c");
			i2c_num = atoi(arg);
		}
		else if (strcmp(arg, "--pc_addr") == 0 || strcmp(arg, "-p") == 0) {
			assert_arg ("--pc_addr");
			slave_addr = atoi(arg);
		}
		else {
			srss_log(LOG_STDOUT, "unrecognized option: %s\n", arg);
			exit (-1);
	        }
	}

	srss_log_fp = fopen(SRSS_LOG_FILE_NAME, "w+");
	if (!srss_log_fp)
		printf("Error in opening log file %s (%s)", SRSS_LOG_FILE_NAME, strerror(errno));

	srss_log(LOG_STDOUT | LOG_FILE, "\n SoC I2C bus number: %d", i2c_num);
	srss_log(LOG_STDOUT | LOG_FILE, "\n External power controller I2C slave address: %d",
		slave_addr);

	/* Open the UIO SRSS device */
	snprintf(dev_name, MAX_NAME_LENGTH, "/dev/%s", srss_dev);
	fd_srss = open (dev_name, (O_RDWR | O_SYNC));
	if (fd_srss < 0) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n Error opening SRSS device %s", dev_name);
		goto end_close;
	}
	if (debug)
		srss_log(LOG_STDOUT | LOG_FILE, "\n Opened device %s, fd: %d\n", dev_name, fd_srss);

	ret = uioutil_get_device(srss_dev, class_name, 32);
	if ( ret < 0 ) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n Error getting uio device %s", srss_dev);
		goto end_close1;
	}
	if (debug)
		srss_log(LOG_STDOUT | LOG_FILE, "\n Class name: %s\n", class_name);

	snprintf(dev_name, MAX_NAME_LENGTH, "/dev/%s", class_name);
	fd_uio = open (dev_name, (O_RDWR | O_SYNC));
	if (fd_uio < 0) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n Error opening uio device %s", dev_name);
		goto end_close1;
	}
	if (debug)
		srss_log(LOG_STDOUT | LOG_FILE, "\n Opened device %s, fd: %d\n", dev_name, fd_uio);
	FD_ZERO(&fds);
	FD_SET(fd_uio, &fds);

	/* Open the I2C device */
	snprintf(dev_name, MAX_LINE_LENGTH, "/dev/i2c-%d", i2c_num);
	fd_i2c = open(dev_name, O_RDWR);
	if (fd_i2c < 0) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n Error opening i2c device %s", dev_name);
		goto end_close2;
	}
	if (debug)
		srss_log(LOG_STDOUT | LOG_FILE, "\n Opened device %s, fd: %d\n", dev_name, fd_i2c);

	/* Set the I2C slave device address */
        if(ioctl(fd_i2c, I2C_SLAVE, slave_addr) < 0) {
                srss_log(LOG_STDOUT | LOG_FILE, "\n Failed to set slave address: %d, error: %s\n",
			slave_addr, strerror(errno));
                goto end_close3;
        }
	/* Enable I2C checksum */
	if(ioctl(fd_i2c, I2C_PEC, 1) < 0) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n Failed to enable I2C PEC");
                goto end_close3;
        }

	/* Get the base address and regs size from the device tree */
	snprintf(file_name, MAX_FILE_NAME_LENGTH,
		"/proc/device-tree/soc/%s/mem", srss_dev);
	if (uioutil_get_ints(file_name, mem, 2) < 0)
		goto end_close3;

	base_addr = (uint32_t)mem[0];

	if (srss_c0_tc_isr(fd_srss, base_addr, fd_i2c) < 0)
		goto end_close3;

	/* Check if SRSS C0 TC is enabled during boot-up */
	if (!srss_c0_tc_enabled(fd_srss, base_addr)) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n SRSS Class 0 TC disabled, "
			"no temperature compensation.\n");
		goto end_close3;
	}

	/* Enable SRSS interrupt */
	srss_c0_tc_interrupt(fd_srss, base_addr, 1);
	value = 1;
	ret = write(fd_uio, &value, sizeof(uint32_t));
	if (ret != sizeof(uint32_t)) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n unable to enable interrupt ");
		goto end_close3;
	}

	for(;;) {
		if (debug) {
			srss_log(LOG_STDOUT | LOG_FILE, "\n waiting for interrupt...\n");
			if (vout_wr) {
				usleep(1000000); /* wait for 1 sec to read
						    back Vout */
				for (i = 0; i < I2C_RETRY_COUNT; i++) { 
					if (i2c_smbus_read_i2c_block_data(fd_i2c,
						PMBUS_READ_VOUT, 2, vout_rb) != -1)
						break;
				}
				if (i == I2C_RETRY_COUNT) {
					srss_log(LOG_STDOUT | LOG_FILE, "\n Error sending  READ_VOUT "
						"PMBus command.\n");
					goto end_close4;
				}

				srss_log(LOG_STDOUT | LOG_FILE, "\n Vout read: %x %x\n",
					vout_rb[0], vout_rb[1]);
				vout_rd = (vout_rb[1] << 8) + vout_rb[0];
				if (vout_wr > vout_rd)
					vout_diff = vout_wr - vout_rd;
				else
					vout_diff = vout_rd - vout_wr;
				if (vout_diff < 3)
					srss_log(LOG_STDOUT | LOG_FILE, "\n PMBus Vout write value "
					     "matches with Vout read value!\n");
				
			}
		}
		ret = select(FD_SETSIZE, &fds, NULL, NULL, NULL);
		if (ret == -1) {
			srss_log(LOG_STDOUT | LOG_FILE, "\n select failed for monitor (error: %s)",
				strerror(errno));
			goto end_close4;
		}
		if (debug)
			clock_gettime(CLOCK_MONOTONIC, &tp_end1);

		/* wait for interrupt */
		read(fd_uio, &value, sizeof(uint32_t));
		if (srss_c0_tc_isr(fd_srss, base_addr, fd_i2c) < 0)
			goto end_close4;
		if (debug) {
			clock_gettime(CLOCK_MONOTONIC, &tp_end2);
			delta_time = clock_diff(&tp_end1, &tp_end2);
			srss_log(LOG_STDOUT | LOG_FILE, "\n interrupt processing time %d ns",delta_time);
		}
	}

end_close4:
	/* Disable interrupt */
	srss_c0_tc_interrupt(fd_srss, base_addr, 0);
	value = 0;
	ret = write(fd_uio, &value, sizeof(uint32_t));
	if (ret != sizeof(uint32_t)) {
		srss_log(LOG_STDOUT | LOG_FILE, "\n unable to disable interrupt ");
	}
end_close3:
	close(fd_i2c);
end_close2:
	close(fd_uio);
end_close1:
	close(fd_srss);
end_close:
	return 0;
}
